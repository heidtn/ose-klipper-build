#!/bin/sh
# On failure, exit
set -e

# ref: https://askubuntu.com/a/970898
if ! [ $(id -u) = 0 ]; then
   echo "The script need to be run as root." >&2
   exit 1
fi

if [ $SUDO_USER ]; then
    real_user=$SUDO_USER
else
    real_user=$(whoami)
fi

# Clone Klipper first
echo "Cloning Klipper..."
sudo -u $real_user git clone https://github.com/Klipper3d/klipper
echo "Installing Klipper for Beaglebone"
sudo -u $real_user ./klipper/scripts/install-beaglebone.sh

# Installing OctoPrint
echo "Cloning and installing octoprint"
sudo -u $real_user git clone https://github.com/foosel/OctoPrint.git
sudo -u $real_user cd OctoPrint/
sudo -u $real_user virtualenv venv
sudo -u $real_user ./venv/bin/python setup.py install

# Configure octoprint to run at startup
cp ~/OctoPrint/scripts/octoprint.init /etc/init.d/octoprint
chmod +x /etc/init.d/octoprint
cp ~/OctoPrint/scripts/octoprint.default /etc/default/octoprint
update-rc.d octoprint defaults

# start octoprint
systemctl start octoprint

