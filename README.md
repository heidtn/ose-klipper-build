# OSE Klipper Build
This repo captures the general build steps and tools required to build the various OSE Klipper images.


# How to Use
First clone the repo
`git clone https://gitlab.com/heidtn/ose-klipper-build.git`

run configure.sh:
`./configure.sh`

configure Octoprint:

It is necessary to modify OctoPrint's /etc/default/octoprint configuration file. One must changethe OCTOPRINT_USER user to debian, change NICELEVEL to 0, uncomment the BASEDIR, CONFIGFILE, and DAEMON settings and change the references from /home/pi/ to /home/debian/

You'll still need to flash the firmware.  The config file is found here, called D3D.cfg.  Steps to flash the firmware are here:

https://www.klipper3d.org/Installation.html#building-and-flashing-the-micro-controller

# TODOs
- create the octoprint config file so it can be copied properly

# Future Work
Eventually it will be best to generate dedicated SD card images so none of this setup is necessary, but that process can be complex. 


